mkdir panel
cd panel

kikit panelize  --layout "grid; rows: 2; cols: 1; space: 2mm"  --tabs "fixed; width: 10mm; vcount: 2; hcount: 2" --cuts "mousebites; drill: 0.5mm; spacing: 1mm; offset: 0mm; prolong: 0.5mm" --post "millradius: 0.5mm"   --source "tolerance: 20mm"  ../auxsupply.kicad_pcb panel.kicad_pcb

kikit fab jlcpcb --assembly --schematic ../auxsupply.kicad_sch ../auxsupply.kicad_pcb gerber_single/ --no-drc
kikit fab jlcpcb --assembly --schematic ../auxsupply.kicad_sch panel.kicad_pcb gerber_panels/ --no-drc